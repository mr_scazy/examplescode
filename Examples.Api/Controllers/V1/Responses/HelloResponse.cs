namespace Examples.Api.Controllers.V1.Responses
{
    public class HelloResponse
    {
        public string Message { get; set; }
    }
}