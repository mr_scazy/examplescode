using Examples.Infrastructure.UniqueRequest;
using Microsoft.AspNetCore.Http;

namespace Examples.Api.Controllers.V1.Requests
{
    public class HelloRequest : IUniqueRequest
    {
        public string Target { get; set; }

        public string UniqueRequestKey(HttpContext context) => Target;
    }
}