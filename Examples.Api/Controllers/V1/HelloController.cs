using System.Threading.Tasks;
using Examples.Api.Controllers.V1.Requests;
using Examples.Api.Controllers.V1.Responses;
using Examples.Infrastructure.UniqueRequest;
using Microsoft.AspNetCore.Mvc;

namespace Examples.Api.Controllers.V1
{
    [ApiController]
    [Route("v1/[controller]")]
    public class HelloController : ControllerBase
    {
        [UniqueRequest(3000, StageRemoteIpAddress = false)]
        [HttpGet]
        public async Task<HelloResponse> Get(HelloRequest request)
        {
            return new HelloResponse
            {
                Message = $"Hello, {request.Target}!"
            };
        }
    }
}