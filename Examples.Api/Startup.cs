using System.Data.SqlClient;
using Examples.Core.Configuration.Sections;
using Examples.DataAccess.Redis;
using Examples.DataAccess.Redis.Impl;
using Examples.DataAccess.SqlServer;
using Examples.DataAccess.SqlServer.Impl;
using Examples.Infrastructure.UniqueRequest;
using Examples.Infrastructure.UniqueRequest.Impl;
using Examples.Services.Blacklist;
using Examples.Services.Blacklist.Impl;
using Examples.Services.UniqueRequest.Impl;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace Examples.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.Configure<RedisSection>(Configuration.GetSection("Redis"));
            services.Configure<UniqueRequestSection>(Configuration.GetSection("UniqueRequest"));

            services.AddSingleton<IRedisConnectionFactory, RedisConnectionFactory>();
            
            services.AddScoped<IUniqueRequestCache, UniqueRequestCompositeCache>();
            services.AddScoped<UniqueRequestRedisCache>();
            services.AddSingleton<UniqueRequestMemoryCache>().AddMemoryCache();
            
            services.AddTransient<IBlacklistedWordService, BlacklistedWordService>();
            services.AddTransient<IBlacklistedWordRepository, BlacklistedWordRepository>(provider =>
            {
                return new BlacklistedWordRepository(() =>
                {
                    var connectionString = provider.GetRequiredService<IOptionsMonitor<ConnectionStringSection>>()
                        .CurrentValue
                        .AntifraudDb;
                    
                    var connection = new SqlConnection(connectionString);
                    connection.Open();
                    return connection;
                });
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}