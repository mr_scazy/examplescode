using StackExchange.Redis;

namespace Examples.DataAccess.Redis
{
    /// <summary>
    /// Сервис взаимодействия с Redis
    /// </summary>
    public interface IRedisConnectionFactory
    {
        /// <summary>
        /// Соединение с Redis
        /// </summary>
        ConnectionMultiplexer GetConnection();
    }
}