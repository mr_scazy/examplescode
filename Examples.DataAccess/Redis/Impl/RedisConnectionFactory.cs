using Examples.Core.Configuration.Sections;
using Microsoft.Extensions.Options;
using StackExchange.Redis;

namespace Examples.DataAccess.Redis.Impl
{
    /// <inheritdoc cref="IRedisConnectionFactory"/>
    public class RedisConnectionFactory : IRedisConnectionFactory
    {
        private readonly IOptionsMonitor<RedisSection> _redisSection;

        private ConnectionMultiplexer _connection;

        /// <summary>
        /// Ctor
        /// </summary>
        public RedisConnectionFactory(IOptionsMonitor<RedisSection> redisSection)
        {
            _redisSection = redisSection;
        }
        
        /// <inheritdoc cref="IRedisConnectionFactory.GetConnection"/>
        public ConnectionMultiplexer GetConnection()
        {
            return _connection ??= ConnectionMultiplexer.Connect(CreateConfigurationOptions());
        }
        
        private ConfigurationOptions CreateConfigurationOptions() => new ConfigurationOptions
        {
            EndPoints =
            {
                _redisSection.CurrentValue.Url
            },
            Password = _redisSection.CurrentValue.Password,
            SyncTimeout = _redisSection.CurrentValue.SyncTimeout
        };
    }
}