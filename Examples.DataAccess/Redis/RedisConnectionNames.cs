namespace Examples.DataAccess.Redis
{
    /// <summary>
    /// Наименования подключений к Redis
    /// </summary>
    public static class RedisConnectionNames
    {
        /// <summary>
        /// Подключение для работы с уникальными запросами
        /// </summary>
        public const string UniqueRequest = "UniqueRequest";
    }
}