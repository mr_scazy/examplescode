﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Examples.Core.Entities.BlackList;
using Examples.Core.Enums;

namespace Examples.DataAccess.SqlServer
{
    /// <summary>
    /// Репозиторий пользователя добавленного в черный список
    /// </summary>
    public interface IBlacklistedWordRepository
    {
        /// <summary>
        /// Получить из черного списка
        /// </summary>
        Task<IReadOnlyCollection<BlacklistedWord>> GetBlacklistsAsync();

        /// <summary>
        /// Получить элементы черного списка и общее количество, используя фильтр
        /// </summary>
        /// <param name="page">Номер страницы</param>
        /// <param name="count">Количество записей на странице</param>
        /// <param name="type">Фильтрация по типу</param>
        /// <param name="department">Фильтрация по учреждению</param>
        /// <param name="word">Фильтрация по слову</param>
        /// <returns></returns>
        Task<(int totalCount, BlacklistedWord[] result)> GetAsync(
            int page = 1,
            int count = 20,
            BlacklistedWordType? type = null,
            string? department = null,
            string? word = null);

        /// <summary>
        /// Добавить слово в черный список
        /// </summary>
        /// <param name="blacklistedWord">Слово, добавляемое в черный список</param>
        Task AddBlacklistAsync(BlacklistedWord blacklistedWord);

        /// <summary>
        /// Обновить слово в черном списке
        /// </summary>
        /// <param name="blacklistedWord">Слово, обновляемое в черном списке</param>
        Task UpdateBlacklistAsync(BlacklistedWord blacklistedWord);

        /// <summary>
        /// Удалить слово из черного списка
        /// </summary>
        /// <param name="id">Идентификатор слова, обновляемое в черном списке</param>
        Task DeleteBlacklistAsync(long id);
    }
}