﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Examples.Core.Entities.BlackList;
using Examples.Core.Enums;

namespace Examples.DataAccess.SqlServer.Impl
{
    /// <inheritdoc cref="IBlacklistedWordRepository"/>
    public class BlacklistedWordRepository : IBlacklistedWordRepository
    {
        private readonly Func<IDbConnection> _connectionFactory;

        /// <summary>
        /// Ctor
        /// </summary>
        public BlacklistedWordRepository(Func<IDbConnection> connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        /// <inheritdoc cref="IBlacklistedWordRepository.GetBlacklistsAsync"/>
        public async Task<IReadOnlyCollection<BlacklistedWord>> GetBlacklistsAsync()
        {
            using var connection = _connectionFactory();

            const string sql = @"
SELECT [Id]
      ,[Word]
      ,[Type]
      ,[Comment]
      ,[Department]
FROM BlacklistedWords WHERE [IsActive]=1";

            var result = await connection.QueryAsync<BlacklistedWord>(sql);

            return result.ToList().AsReadOnly();
        }

        /// <inheritdoc cref="IBlacklistedWordRepository.GetAsync"/>
        public async Task<(int totalCount, BlacklistedWord[] result)> GetAsync(
            int page = 1,
            int count = 20,
            BlacklistedWordType? type = null,
            string? department = null,
            string? word = null)
        {
            using var connection = _connectionFactory();

            const string dataSqlBase = "SELECT * FROM [BlacklistedWords]";

            const string countSqlBase = "SELECT COUNT (*) from [BlacklistedWords]";

            var filter = new StringBuilder(" WHERE [IsActive]=1");

            if (type.HasValue)
            {
                filter.Append(" AND [Type] = @Type");
            }

            if (!string.IsNullOrWhiteSpace(department))
            {
                filter.Append(" AND [Department] = @Department");
            }

            if (!string.IsNullOrWhiteSpace(word))
            {
                filter.Append(" AND [Word] = @Word");
            }

            const string paging = " ORDER BY [CreatedDate] DESC OFFSET @Skip ROWS FETCH NEXT @Take ROWS ONLY";

            var dataSql = $"{dataSqlBase}{filter}{paging}";

            var countSql = $"{countSqlBase}{filter}";

            var data = await connection.QueryAsync<BlacklistedWord>(
                dataSql,
                new
                {
                    Skip = (page - 1) * count,
                    Take = count,
                    Type = type,
                    Department = department,
                    Word = word
                });

            var totalCount = await connection.ExecuteScalarAsync<int>(
                countSql,
                new
                {
                    Type = type,
                    Department = department,
                    Word = word
                });

            return (totalCount, data.ToArray());
        }

        /// <inheritdoc cref="IBlacklistedWordRepository.AddBlacklistAsync"/>
        public async Task AddBlacklistAsync(BlacklistedWord blacklistedWord)
        {
            using var connection = _connectionFactory();

            const string sql = @"
INSERT INTO [BlacklistedWords]
           ([Type]
           ,[Word]
           ,[IsActive]
           ,[Comment]
           ,[Department]
           ,[CreatedDate]
           ,[CreatedBy])
     VALUES
           (@Type
           ,@Word
           ,@IsActive
           ,@Comment
           ,@Department
           ,@CreatedDate
           ,@CreatedBy)";

            var createdDate = DateTime.UtcNow;

            await connection.ExecuteScalarAsync(
                sql, 
                new
                {
                    blacklistedWord.Type,
                    blacklistedWord.Word,
                    IsActive = true,
                    blacklistedWord.Comment,
                    blacklistedWord.Department,
                    CreatedDate = createdDate,
                    blacklistedWord.CreatedBy
                });
        }

        /// <inheritdoc cref="IBlacklistedWordRepository.UpdateBlacklistAsync"/>
        public async Task UpdateBlacklistAsync(BlacklistedWord blacklistedWord)
        {
            using var connection = _connectionFactory();
            using var transaction = connection.BeginTransaction();
            
            const string insertSql = @"
INSERT INTO [BlacklistedWords]
           ([Type]
           ,[Word]
           ,[IsActive]
           ,[Comment]
           ,[Department]
           ,[CreatedDate]
           ,[CreatedBy])
     OUTPUT INSERTED.Id
     VALUES
           (@Type
           ,@Word
           ,@IsActive
           ,@Comment
           ,@Department
           ,@CreatedDate
           ,@CreatedBy)";
            
            var now = DateTime.UtcNow;

            var insertedId = await connection.QuerySingleAsync<long>(
                insertSql,
                new
                {
                    blacklistedWord.Type,
                    blacklistedWord.Word,
                    IsActive = true,
                    blacklistedWord.Comment,
                    blacklistedWord.Department,
                    CreatedDate = now,
                    blacklistedWord.CreatedBy
                },
                transaction);

            const string updateSql = @"
UPDATE [BlacklistedWords]
   SET [ChangedDate] = @ChangedDate
      ,[IsActive] = 0
      ,[ParentId] = @ParentId
 WHERE [Id] = @Id";
            
            await connection.ExecuteAsync(
                updateSql,
                new
                {
                    blacklistedWord.Id,
                    ChangedDate = now,
                    ParentId = insertedId,
                },
                transaction);
                
            transaction.Commit();

            // обновляем модель новыми данными
            blacklistedWord.Id = insertedId;
            blacklistedWord.CreatedDate = now;
        }

        /// <inheritdoc cref="IBlacklistedWordRepository.DeleteBlacklistAsync"/>
        public async Task DeleteBlacklistAsync(long id)
        {
            using var connection = _connectionFactory();
            
            var now = DateTime.UtcNow;
            
            const string sql = @"
UPDATE [BlacklistedWords] SET 
     [ChangedDate] = @ChangedDate,
     [IsActive] = 0
WHERE Id = @Id";
            
            await connection.ExecuteAsync(
                sql, 
                new
                {
                    Id = id,
                    ChangedDate = now
                });
        }
    }
}