using Microsoft.AspNetCore.Http;

namespace Examples.Infrastructure.Extensions
{
    /// <summary>
    /// Методы-расширения для <see cref="HttpContext"/>
    /// </summary>
    public static class HttpContextExtensions
    {
        /// <summary>
        /// Получить IP адрес удаленного хоста
        /// </summary>
        public static string GetRemoteIpAddress(this HttpContext context)
        {
            var xRealIp = context?.Request?.Headers["X-Real-IP"].ToString();
            return string.IsNullOrWhiteSpace(xRealIp) 
                ? context?.Connection?.RemoteIpAddress?.ToString() 
                : xRealIp;
        }
    }
}