using Microsoft.AspNetCore.Http;

namespace Examples.Infrastructure.UniqueRequest
{
    /// <summary>
    /// Уникальный запрос
    /// </summary>
    public interface IUniqueRequest
    {
        /// <summary>
        /// Ключ уникального запроса
        /// </summary>
        string UniqueRequestKey(HttpContext context);
    }
}