using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace Examples.Infrastructure.UniqueRequest.Impl
{
    /// <summary>
    /// Кэш уникальных запросов в памяти
    /// </summary>
    public class UniqueRequestMemoryCache : IUniqueRequestCache
    {
        private readonly IMemoryCache _memoryCache;

        /// <summary>
        /// Ctor
        /// </summary>
        public UniqueRequestMemoryCache(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }
        
        /// <inheritdoc cref="IUniqueRequestCache.IsExistAsync"/>
        public Task<bool> IsExistAsync(string key)
        {
            key = key ?? throw new ArgumentNullException(nameof(key));
            return Task.FromResult(_memoryCache.TryGetValue(key, out _));
        }

        /// <inheritdoc cref="IUniqueRequestCache.IsExistAsync"/>
        public virtual Task SetAsync(string key, int lifetime)
        {
            key = key ?? throw new ArgumentNullException(nameof(key));
            _memoryCache.Remove(key);
            _memoryCache.Set(key, lifetime, DateTime.UtcNow.AddMilliseconds(lifetime));
            return Task.CompletedTask;
        }

        /// <inheritdoc cref="IUniqueRequestCache.GetRemainingLifeTimeAsync"/>
        public Task<int?> GetRemainingLifeTimeAsync(string key)
        {
            if (_memoryCache.TryGetValue(key, out var value))
            {
                return Task.FromResult(default(int?));
            }

            if (!(value is DateTime expireDate))
            {
                return Task.FromResult(default(int?));
            }
            
            var lifetime = (int)(expireDate - DateTime.UtcNow).TotalMilliseconds;

            return Task.FromResult(lifetime > 0 
                ? lifetime 
                : default(int?));
        }
    }
}