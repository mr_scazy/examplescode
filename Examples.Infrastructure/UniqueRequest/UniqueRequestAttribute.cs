using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Examples.Infrastructure.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace Examples.Infrastructure.UniqueRequest
{
    /// <summary>
    /// Атрибут для проверки запроса на уникальность
    /// </summary>
    public class UniqueRequestAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Отслеживать IP адрес (по умолчанию - true)
        /// </summary>
        public bool StageRemoteIpAddress { get; set; } = true;
        
        private readonly int _milliseconds;

        /// <summary>
        /// Атрибут для проверки запроса на уникальность
        /// </summary>
        /// <param name="milliseconds">Время жизни уникальности запроса в миллисекундах</param>
        public UniqueRequestAttribute(int milliseconds = 5000)
        {
            _milliseconds = milliseconds;
        }

        /// <inheritdoc cref="ActionFilterAttribute.OnActionExecutionAsync"/>
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var requests = context.ActionArguments
                .Select(x => x.Value as IUniqueRequest)
                .Where(x => x != null)
                .ToArray();

            if (requests.Length < 1)
            {
                await base.OnActionExecutionAsync(context, next);
                return;
            }
            
            var cache = context.HttpContext.RequestServices.GetService<IUniqueRequestCache>();
            if (cache == null)
            {
                await base.OnActionExecutionAsync(context, next);
                return;
            }

            var prefix = GetPrefix(context);

            foreach (var request in requests)
            {
                var key = $"{prefix}#{request.UniqueRequestKey(context.HttpContext)}";

                if (await cache.IsExistAsync(key))
                {
                    context.Result = new ObjectResult(new
                    {
                        success = false,
                        message = "Слишком много запросов"
                    })
                    {
                        StatusCode = (int) HttpStatusCode.TooManyRequests
                    };
                    return;
                }
                
                await cache.SetAsync(key, _milliseconds);
            }

            await base.OnActionExecutionAsync(context, next);
        }

        private string GetPrefix(ActionContext context)
        {
            var prefixBuilder = new StringBuilder();
            if (StageRemoteIpAddress)
            {
                prefixBuilder.Append(context.HttpContext.GetRemoteIpAddress());
                prefixBuilder.Append("=>");
            }

            prefixBuilder.Append(context.HttpContext.Request.Method);
            prefixBuilder.Append("@");
            prefixBuilder.Append(context.HttpContext.Request.Path);

            var prefix = prefixBuilder.ToString();
            return prefix;
        }
    }
}