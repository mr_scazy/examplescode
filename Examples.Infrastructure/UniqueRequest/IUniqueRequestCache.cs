using System.Threading.Tasks;

namespace Examples.Infrastructure.UniqueRequest
{
    /// <summary>
    /// Кэш уникальных запросов
    /// </summary>
    public interface IUniqueRequestCache
    {
        /// <summary>
        /// Проверка на наличие уникального запроса
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<bool> IsExistAsync(string key);

        /// <summary>
        /// Установить ключ с временем жизни уникального запроса
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="lifetime">Время жизни запроса</param>
        Task SetAsync(string key, int lifetime);

        /// <summary>
        /// Получить оставшее время жизни уникального запроса
        /// </summary>
        /// <param name="key">Ключ</param>
        Task<int?> GetRemainingLifeTimeAsync(string key);
    }
}