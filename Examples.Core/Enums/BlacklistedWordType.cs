namespace Examples.Core.Enums
{
    /// <summary>
    /// Тип слова черного списка
    /// </summary>
    public enum BlacklistedWordType
    {
        /// <summary>
        /// Имя
        /// </summary>
        Name = 1,
        
        /// <summary>
        /// Наименование компании
        /// </summary>
        CompanyName = 2,
        
        /// <summary>
        /// Сайт
        /// </summary>
        Website = 4,
        
        /// <summary>
        /// Электронная почта
        /// </summary>
        Email = 3
    }
}