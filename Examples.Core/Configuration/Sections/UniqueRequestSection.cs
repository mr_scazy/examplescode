namespace Examples.Core.Configuration.Sections
{
    /// <summary>
    /// Настройки уникальности запроса
    /// </summary>
    public class UniqueRequestSection
    {
        /// <summary>
        /// Использовать хранилище в памяти
        /// </summary>
        public bool UseMemoryCache { get; set; }
        
        /// <summary>
        /// Использовать хранилище в Redis
        /// </summary>
        public bool UseRedisCache { get; set; }
    }
}