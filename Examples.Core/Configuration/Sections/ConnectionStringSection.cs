namespace Examples.Core.Configuration.Sections
{
    /// <summary>
    /// Секция со строками подключения
    /// </summary>
    public class ConnectionStringSection
    {
        public string AntifraudDb { get; set; } = string.Empty;
    }
}