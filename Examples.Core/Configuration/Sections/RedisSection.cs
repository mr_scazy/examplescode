namespace Examples.Core.Configuration.Sections
{
    /// <summary>
    /// Секция конфигурации Redis
    /// </summary>
    public class RedisSection
    {
        /// <summary>
        /// URL редиса
        /// </summary>
        public string Url { get; set; } = string.Empty;
        
        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; } = string.Empty;

        /// <summary>
        /// Таймаут в миллисекундах (по умолчанию 20 секунд)
        /// </summary>
        public int SyncTimeout { get; set; } = 20000;
    }
}