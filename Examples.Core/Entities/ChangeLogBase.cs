#nullable enable
using System;

namespace Examples.Core.Entities
{
    /// <summary>
    /// Базовая сущность с полями отслеживания изменений
    /// </summary>
    public abstract class ChangeLogBase
    {
        /// <summary>
        /// Дата добавления
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime? ChangedDate { get; set; }

        /// <summary>
        /// Признак активности записи
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Идентификатор родителькой записи
        /// </summary>
        public long? ParentId { get; set; }

        /// <summary>
        /// Идентификатор пользователя, создавшего запись
        /// </summary>
        public Guid? CreatedBy { get; set; }
        
        /// <summary>
        /// Комментарий
        /// </summary>
        public string? Comment { get; set; }
        
        /// <summary>
        /// Департамент
        /// </summary>
        public string? Department { get; set; }
    }
}