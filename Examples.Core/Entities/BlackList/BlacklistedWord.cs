﻿using System.ComponentModel.DataAnnotations;
using Examples.Core.Enums;

namespace Examples.Core.Entities.BlackList
{
    /// <summary>
    /// Слово добавленное в черный список
    /// </summary>
    public class BlacklistedWord : ChangeLogBase
    {
        /// <summary>
        /// Идентификатор элемента черного списка
        /// </summary>
        [Key]
        public long Id { get; set; }

        /// <summary>
        /// Значение слова из черного списка
        /// </summary>
        public string Word { get; set; } = string.Empty;

        /// <summary>
        /// Тип слова черного списка
        /// </summary>
        public BlacklistedWordType Type { get; set; }  
    }
}