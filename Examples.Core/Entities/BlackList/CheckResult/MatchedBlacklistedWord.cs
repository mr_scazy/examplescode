namespace Examples.Core.Entities.BlackList.CheckResult
{
    /// <summary>
    /// Слова, попавшие на совпадение по черному списку
    /// </summary>
    public class MatchedBlacklistedWord
    {
        /// <summary>
        /// Название правила
        /// </summary>
        public string RuleName { get; set; } = string.Empty;

        /// <summary>
        /// Переданные данные
        /// </summary>
        public string PassedData { get; set; } = string.Empty;

        /// <summary>
        /// Данные в черном списке
        /// </summary>
        public string BlacklistedData { get; set; } = string.Empty;
    }
}