using System.Threading.Tasks;
using Examples.Services.Blacklist.DTO;

namespace Examples.Services.Blacklist
{
    /// <summary>
    /// Сервис для работы с черным списком слов
    /// </summary>
    public interface IBlacklistedWordService
    {
        /// <summary>
        /// Получение информации по черому списку
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<BlacklistWordItemsResult> GetBlacklist(GetWordBlackListDto model);
        
        /// <summary>
        /// Добавить пользователя в черный список
        /// </summary>
        /// <param name="model">Данные слова, доавляемого в черный список</param>
        Task AddBlacklistItemAsync(BlacklistedWordInsertDto model);
        
        /// <summary>
        /// Обновить пользователя в черном списке
        /// </summary>
        /// <param name="model">Данные слова для обновления</param>
        Task UpdateBlacklistItemAsync(BlacklistedWordUpdateDto model);
        
        /// <summary>
        /// Удалить данные из черного списка
        /// </summary>
        /// <param name="id">Идентификатор записи для удаления</param>
        Task DeleteBlacklistItemAsync(long id);
    }
}