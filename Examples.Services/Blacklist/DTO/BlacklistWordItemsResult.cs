namespace Examples.Services.Blacklist.DTO
{
    /// <summary>
    /// Результат черного списка слов
    /// </summary>
    public class BlacklistWordItemsResult
    {
        /// <summary>
        /// Элементы черного списка
        /// </summary>
        public BlacklistWordItemDto[]? Items { get; set; }
        
        /// <summary>
        /// Общее число элементов
        /// </summary>
        public int TotalCount { get; set; }
    }
}