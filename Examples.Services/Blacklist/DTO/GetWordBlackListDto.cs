using Examples.Core.Enums;

namespace Examples.Services.Blacklist.DTO
{
    /// <summary>
    /// Модель для получения списка элементов черного списка слов
    /// </summary>
    public class GetWordBlackListDto
    {
        /// <summary>
        /// Номер страницы
        /// </summary>
        public int Page { get; set; }
        
        /// <summary>
        /// Количество записей на странице
        /// </summary>
        public int Count { get; set; }
        
        /// <summary>
        /// Фильтр по типу
        /// </summary>
        public BlacklistedWordType? Type { get; set; }
        
        /// <summary>
        /// Фильт по учреждению
        /// </summary>
        public string? Department { get; set; }
        
        /// <summary>
        /// Фильтр по слову
        /// </summary>
        public string? Word { get; set; }
    }
}