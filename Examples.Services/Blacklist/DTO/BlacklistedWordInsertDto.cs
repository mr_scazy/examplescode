using System;
using Examples.Core.Enums;

namespace Examples.Services.Blacklist.DTO
{
    /// <summary>
    /// Модель для добавления данных черного списка слов 
    /// </summary>
    public class BlacklistedWordInsertDto
    {
        /// <summary>
        /// Значение слова из черного списка
        /// </summary>
        public string Word { get; set; } = string.Empty;

        /// <summary>
        /// Тип слова черного списка
        /// </summary>
        public BlacklistedWordType Type { get; set; }
        
        /// <summary>
        /// Комментарий
        /// </summary>
        public string? Comment { get; set; }
        
        /// <summary>
        /// Департамент
        /// </summary>
        public string? Department { get; set; }
        
        /// <summary>
        /// Идентификатор пользователя, создавшего запись
        /// </summary>
        public Guid? CreatedBy { get; set; }
    }
}