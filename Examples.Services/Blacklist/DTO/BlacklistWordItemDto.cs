using System;
using Examples.Core.Enums;

namespace Examples.Services.Blacklist.DTO
{
    /// <summary>
    /// Элемент из черного списка слов
    /// </summary>
    public class BlacklistWordItemDto
    {
        /// <summary>
        /// Идентификатор элемента черного списка
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Значение слова из черного списка
        /// </summary>
        public string Word { get; set; } = string.Empty;

        /// <summary>
        /// Тип слова черного списка
        /// </summary>
        public BlacklistedWordType Type { get; set; }
        
        /// <summary>
        /// Комментарий
        /// </summary>
        public string? Comment { get; set; }
        
        /// <summary>
        /// Департамент
        /// </summary>
        public string? Department { get; set; }
        
        /// <summary>
        /// Дата добавления
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime? ChangedDate { get; set; }

        /// <summary>
        /// Признак активности записи
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Идентификатор родителькой записи
        /// </summary>
        public long? ParentId { get; set; }

        /// <summary>
        /// Идентификатор пользователя, создавшего запись
        /// </summary>
        public Guid? CreatedBy { get; set; }
    }
}