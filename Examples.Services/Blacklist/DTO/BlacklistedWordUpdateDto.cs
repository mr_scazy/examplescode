using System;
using Examples.Core.Enums;

namespace Examples.Services.Blacklist.DTO
{
    /// <summary>
    /// Модель для обновления данных черного списка слов 
    /// </summary>
    public class BlacklistedWordUpdateDto
    {
        /// <summary>
        /// Идентификатор слова в черном списке
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// Значение слова из черного списка
        /// </summary>
        public string Word { get; set; } = string.Empty;

        /// <summary>
        /// Тип слова черного списка
        /// </summary>
        public BlacklistedWordType Type { get; set; }
        
        /// <summary>
        /// Комментарий
        /// </summary>
        public string? Comment { get; set; }
        
        /// <summary>
        /// Департамент
        /// </summary>
        public string? Department { get; set; }
        
        /// <summary>
        /// Идентификатор пользователя, создавшего запись
        /// </summary>
        public Guid? CreatedBy { get; set; }
    }
}