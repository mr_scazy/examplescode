using System.Linq;
using System.Threading.Tasks;
using Examples.Core.Entities.BlackList;
using Examples.DataAccess.SqlServer;
using Examples.Services.Blacklist.DTO;

namespace Examples.Services.Blacklist.Impl
{
    /// <inheritdoc cref="IBlacklistedWordService"/>
    public class BlacklistedWordService : IBlacklistedWordService
    {
        private readonly IBlacklistedWordRepository _blacklistedWordRepository;

        /// <summary>
        /// Ctor
        /// </summary>
        public BlacklistedWordService(IBlacklistedWordRepository blacklistedWordRepository)
        {
            _blacklistedWordRepository = blacklistedWordRepository;
        }
        
        /// <inheritdoc cref="IBlacklistedWordService.GetBlacklist"/>
        public async Task<BlacklistWordItemsResult> GetBlacklist(GetWordBlackListDto model)
        {
            var (totalCount, blacklistedUsers) = await _blacklistedWordRepository.GetAsync(
                model.Page,
                model.Count,
                model.Type,
                model.Department,
                model.Word);

            var result = 
                new BlacklistWordItemsResult
                {
                    Items = blacklistedUsers
                        .Select(
                            x => new BlacklistWordItemDto
                            {
                                Id = x.Id,
                                Word = x.Word,
                                Type = x.Type,
                                Department = x.Department,
                                Comment = x.Comment,
                                IsActive = x.IsActive,
                                ParentId = x.ParentId,
                                CreatedBy = x.CreatedBy,
                                CreatedDate = x.CreatedDate,
                                ChangedDate = x.ChangedDate
                            })
                        .ToArray(),
                    TotalCount = totalCount
                };

            return result;
        }

        /// <inheritdoc cref="IBlacklistedWordService.AddBlacklistItemAsync"/>
        public async Task AddBlacklistItemAsync(BlacklistedWordInsertDto model)
        {
            var blackListedWord = new BlacklistedWord
            {
                Word = model.Word,
                CreatedBy = model.CreatedBy,
                Type = model.Type,
                Comment = model.Comment,
                Department = model.Department
            };
            
            await _blacklistedWordRepository.AddBlacklistAsync(blackListedWord);
        }

        /// <inheritdoc cref="IBlacklistedWordService.UpdateBlacklistItemAsync"/>
        public async Task UpdateBlacklistItemAsync(BlacklistedWordUpdateDto model)
        {
            var blackListedWord = new BlacklistedWord
            {
                Id = model.Id,
                Word = model.Word,
                CreatedBy = model.CreatedBy,
                Type = model.Type,
                Comment = model.Comment,
                Department = model.Department
            };

            await _blacklistedWordRepository.UpdateBlacklistAsync(blackListedWord);
        }

        /// <inheritdoc cref="IBlacklistedWordService.DeleteBlacklistItemAsync"/>
        public async Task DeleteBlacklistItemAsync(long id)
        {
            await _blacklistedWordRepository.DeleteBlacklistAsync(id);
        }
    }
}