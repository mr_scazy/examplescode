using System;
using System.Threading.Tasks;
using Examples.Core.Configuration.Sections;
using Examples.Infrastructure.UniqueRequest;
using Examples.Infrastructure.UniqueRequest.Impl;
using Microsoft.Extensions.Options;

namespace Examples.Services.UniqueRequest.Impl
{
    /// <summary>
    /// Кэш хранилище уникальных запросов
    /// </summary>
    public class UniqueRequestCompositeCache : IUniqueRequestCache
    {
        private readonly IOptionsSnapshot<UniqueRequestSection> _options;
        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// Ctor
        /// </summary>
        public UniqueRequestCompositeCache(IOptionsSnapshot<UniqueRequestSection> options, IServiceProvider serviceProvider)
        {
            _options = options;
            _serviceProvider = serviceProvider;
        }
        
        /// <inheritdoc cref="IUniqueRequestCache.IsExistAsync"/>
        public async Task<bool> IsExistAsync(string key)
        {
            var opt = _options.Value;

            if (opt.UseMemoryCache)
            {
                var cache = GetCache<UniqueRequestMemoryCache>();
                if (await cache.IsExistAsync(key))
                {
                    return true;
                }
            }

            if (opt.UseRedisCache)
            {
                var cache = GetCache<UniqueRequestRedisCache>();
                if (await cache.IsExistAsync(key))
                {
                    await UpdateAsync<UniqueRequestMemoryCache>(key, cache, opt.UseMemoryCache);
                    return true;
                }
            }

            return false;
        }

        /// <inheritdoc cref="IUniqueRequestCache.SetAsync"/>
        public async Task SetAsync(string key, int lifetime)
        {
            await SetAsync<UniqueRequestMemoryCache>(key, lifetime, _options.Value.UseMemoryCache);
            await SetAsync<UniqueRequestRedisCache>(key, lifetime, _options.Value.UseRedisCache);
        }

        /// <inheritdoc cref="IUniqueRequestCache.GetRemainingLifeTimeAsync"/>
        public async Task<int?> GetRemainingLifeTimeAsync(string key)
            => await GetRemainingLifeTimeAsync<UniqueRequestMemoryCache>(key, _options.Value.UseMemoryCache) ??
               await GetRemainingLifeTimeAsync<UniqueRequestRedisCache>(key, _options.Value.UseRedisCache);

        #region privates

        private IUniqueRequestCache GetCache<T>() where T : class, IUniqueRequestCache
            => (IUniqueRequestCache) _serviceProvider.GetService(typeof(T));
        
        private async Task UpdateAsync<TTarget>(string key, 
            IUniqueRequestCache source,
            bool useTarget) where TTarget : class, IUniqueRequestCache
        {
            if (!useTarget)
            {
                return;
            }

            var idleTime = await source.GetRemainingLifeTimeAsync(key);
            if (idleTime == null)
            {
                return;
            }

            await GetCache<TTarget>()
                .SetAsync(key, idleTime.Value);
        }

        private Task SetAsync<T>(string key, int lifetime, bool use) 
            where T : class, IUniqueRequestCache => use 
            ? GetCache<T>().SetAsync(key, lifetime) 
            : Task.CompletedTask;

        private Task<int?> GetRemainingLifeTimeAsync<T>(string key, bool use) 
            where T : class, IUniqueRequestCache => use
            ? GetCache<T>().GetRemainingLifeTimeAsync(key) 
            : Task.FromResult<int?>(null);

        #endregion
    }
}