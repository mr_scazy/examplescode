using System;
using System.Threading.Tasks;
using Examples.DataAccess.Redis;
using Examples.Infrastructure.UniqueRequest;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;

namespace Examples.Services.UniqueRequest.Impl
{
    /// <summary>
    /// Кэш уникальных запросов в Redis'е
    /// </summary>
    public class UniqueRequestRedisCache : IUniqueRequestCache
    {
        private readonly ILogger<UniqueRequestRedisCache> _logger;
        private readonly IConnectionMultiplexer? _connection;
        private readonly IDatabase? _database;
        
        /// <summary>
        /// Ctor
        /// </summary>
        public UniqueRequestRedisCache(
            ILogger<UniqueRequestRedisCache> logger,
            IRedisConnectionFactory connectionFactory)
        {
            _logger = logger;

            const string connectionName = RedisConnectionNames.UniqueRequest;

            try
            {
                _connection = connectionFactory.GetConnection();
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, $"Failed to create Redis connection: connectionName={connectionName}");
            }

            _database = _connection?.GetDatabase();
        }
        
        /// <inheritdoc cref="IUniqueRequestCache.IsExistAsync"/>
        public async Task<bool> IsExistAsync(string key)
        {
            if (_database == null)
            {
                return false;
            }
            
            try
            {
                return await _database.KeyExistsAsync(key);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Failed to verify request key in Redis: key={key}");
            }
            
            return false;
        }

        /// <inheritdoc cref="IUniqueRequestCache.SetAsync"/>
        public async Task SetAsync(string key, int lifetime)
        {
            if (_database == null)
            {
                return;
            }
            
            try
            {
                var now = DateTime.UtcNow;
                var ticks = now.AddMilliseconds(lifetime).Ticks.ToString();
                await _database.StringSetAsync(key, ticks);
                await _database.KeyExpireAsync(key, TimeSpan.FromMilliseconds(lifetime));
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Failed to add unique request key to Redis: key={key}");
            }
        }

        /// <inheritdoc cref="IUniqueRequestCache.GetRemainingLifeTimeAsync"/>
        public async Task<int?> GetRemainingLifeTimeAsync(string key)
        {
            if (_database == null)
            {
                return null;
            }

            try
            {
                var value = await _database.StringGetAsync(key);

                if (!value.HasValue)
                {
                    return null;
                }
                
                if (!value.TryParse(out long ticks))
                {
                    return null;
                }

                var date = new DateTime(ticks, DateTimeKind.Utc);
                var now = DateTime.UtcNow;
                var ts = (date - now);
                var ms = (int) ts.TotalMilliseconds;
                return ms > 0 ? ms : default(int?);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Failed to get lifetime by unique request key from Redis: key={key}");
            }

            return null;
        }
    }
}